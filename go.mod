module gitlab.com/kintar1900/elite-journalfile

go 1.14

require (
	github.com/google/addlicense v0.0.0-20200622132530-df58acafd6d5 // indirect
	github.com/jstemmer/go-junit-report v0.9.1 // indirect
	github.com/radovskyb/watcher v1.0.7
	github.com/reactivex/rxgo/v2 v2.1.0
	github.com/stretchr/testify v1.6.1
	golang.org/x/sync v0.0.0-20200625203802-6e8e738ad208 // indirect
)
