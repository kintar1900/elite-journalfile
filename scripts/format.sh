#!/usr/bin/env bash

eval `go env`

go get -u github.com/google/addlicense
find ./ -name '*.go' | xargs $GOPATH/bin/addlicense -f resources/license-template.txt -c "Alec Lanter" -l gplv3
go fmt ./...