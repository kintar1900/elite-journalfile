#!/usr/bin/env bash

# Exit on error
set -e

TARGET=./event/...

mkdir -p cover
mkdir -p report

go get -u github.com/jstemmer/go-junit-report

go test -race -short $TARGET
eval $(go env)
go test -v -covermode=count -coverprofile cover/coverage.cov $TARGET 2>&1 | $GOPATH/bin/go-junit-report -set-exit-code > test-report.xml
go tool cover -func cover/coverage.cov | grep total | awk '{print "total coverage: "$3}'
go tool cover -html=cover/coverage.cov -o cover/coverage.html
