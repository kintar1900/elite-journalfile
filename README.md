# elite-journalfile
[![pipeline status](https://gitlab.com/kintar1900/elite-journalfile/badges/master/pipeline.svg)](https://gitlab.com/kintar1900/elite-journalfile/-/commits/master)
[![coverage report](https://gitlab.com/kintar1900/elite-journalfile/badges/master/coverage.svg)](https://gitlab.com/kintar1900/elite-journalfile/-/commits/master)

A go language library for reading and processing the Elite:Dangerous player journal.