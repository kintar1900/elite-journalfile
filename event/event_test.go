// ed-journal : A Go library for processing Elite:Dangerous journal files
// Copyright (C) 2020 Alec Lanter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package event

import (
	"errors"
	"fmt"
	"github.com/stretchr/testify/assert"
	"os"
	"testing"
	"time"
)

func Test_eventConstructors(t *testing.T) {
	eventTypeCount := len(eventConstructors)

	// TODO: This is a magic string!!!  You have to update it if you add more events!
	expectedEventCount := 16

	assert.Equal(t, expectedEventCount, eventTypeCount, "wrong number of constructors")
}

func Test_ParseOneEvent(t *testing.T) {
	expectedTime, _ := time.Parse(time.RFC3339, "2020-06-28T13:16:14Z")
	json := `{ "timestamp":"2020-06-28T13:16:14Z", "event":"Commander", "FID":"FXXXXX", "Name":"Whooya" }`
	parsed, err := ParseEvent(json)
	assert.Nil(t, err, "error parsing json: ", err)

	evt, ok := parsed.(Event)
	assert.True(t, ok, "could not cast to Event")
	_, ok = parsed.(*Commander)
	assert.True(t, ok, "could not cast to *Commander")

	assert.Equal(t, Name("Commander"), evt.EventName())
	assert.Equal(t, expectedTime, evt.Timestamp())
}

func Test_ParseBadData(t *testing.T) {
	defer recoverPanicAndFail(t)

	_, err := ParseEvent("this { is some, \"garbage")
	assert.NotNil(t, err, "no error was returned for garbage event")
}

func Test_EventNameParsing(t *testing.T) {
	jsonData := []string{
		`{ "event":"FSDJump" }`,
		`{ "event" :"FSDJump" }`,
		`{ "event" : "FSDJump" }`,
		`{ "event": "FSDJump" }`,
		`{ "event":
				"FSDJump" }`,
		`{ "event":
     	"FSDJump" }`,
	}

	for i, data := range jsonData {
		t.Run(fmt.Sprintf("json variant %d", i), func(t *testing.T) {
			_, err := ParseEvent(data)
			assert.Nil(t, err, "failed to parse '%s'", data)
		})
	}
}

func Test_ParseInvalidEvent(t *testing.T) {
	defer recoverPanicAndFail(t)

	_, err := ParseEvent(`{ "timestamp": "2020-01-01T00:00:00Z", "event": "NoSuchEvent"}`)

	assert.True(t, errors.As(err, ErrUnrecognizedEvent), "wrong error returned for unrecognized event: ", err)
}

func recoverPanicAndFail(t *testing.T) {
	r := recover()
	if r != nil {
		t.Fatal("panic: ", r)
	}
}

func Test_ParseFile(t *testing.T) {
	fileName := "../test-data/Journal.200628081428.01.log"
	values, invalids, err := ParseFile(fileName)

	assert.Nil(t, err)

	assert.Equal(t, 6, invalids, "wrong number of invalid events found")
	assert.Equal(t, 5, len(values), "wrong number of events parsed")
}

func Test_ParseBadFile(t *testing.T) {
	_, _, err := ParseFile("D:C:E:NOSUCHFILE")

	_, ok := errors.Unwrap(err).(*os.PathError)

	assert.True(t, ok, "result was not PathError: ", err)
}
