// ed-journal : A Go library for processing Elite:Dangerous journal files
// Copyright (C) 2020 Alec Lanter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package event

type CarrierBuy struct {
	EdEvent
	CarrierID      int64  `json:"CarrierID"`
	BoughtAtMarket int    `json:"BoughtAtMarket"`
	Location       string `json:"Location"`
	SystemAddress  int64  `json:"SystemAddress"`
	Price          int64  `json:"Price"`
	Variant        string `json:"Variant"`
	Callsign       string `json:"Callsign"`
}

const EvtCarrierJump = Name("CarrierJump")

type CarrierJump struct {
	EdEvent
	Docked                       bool               `json:"Docked"`
	StationName                  string             `json:"StationName"`
	StationType                  string             `json:"StationType"`
	MarketID                     int64              `json:"MarketID"`
	StationFaction               StationFaction     `json:"StationFaction"`
	StationGovernment            string             `json:"StationGovernment"`
	StationGovernmentLocalised   string             `json:"StationGovernment_Localised"`
	StationServices              []string           `json:"StationServices"`
	StationEconomy               string             `json:"StationEconomy"`
	StationEconomyLocalised      string             `json:"StationEconomy_Localised"`
	StationEconomies             []StationEconomies `json:"StationEconomies"`
	StarSystem                   string             `json:"StarSystem"`
	SystemAddress                int64              `json:"SystemAddress"`
	StarPos                      []float64          `json:"StarPos"`
	SystemAllegiance             string             `json:"SystemAllegiance"`
	SystemEconomy                string             `json:"SystemEconomy"`
	SystemEconomyLocalised       string             `json:"SystemEconomy_Localised"`
	SystemSecondEconomy          string             `json:"SystemSecondEconomy"`
	SystemSecondEconomyLocalised string             `json:"SystemSecondEconomy_Localised"`
	SystemGovernment             string             `json:"SystemGovernment"`
	SystemGovernmentLocalised    string             `json:"SystemGovernment_Localised"`
	SystemSecurity               string             `json:"SystemSecurity"`
	SystemSecurityLocalised      string             `json:"SystemSecurity_Localised"`
	Population                   int                `json:"Population"`
	Body                         string             `json:"Body"`
	BodyID                       int                `json:"BodyID"`
	BodyType                     string             `json:"BodyType"`
}

const EvtCarrierJumpRequest = Name("CarrierJumpRequest")

type CarrierJumpRequest struct {
	EdEvent
	CarrierID     int64  `json:"CarrierID"`
	SystemName    string `json:"SystemName"`
	Body          string `json:"Body"`
	SystemAddress int64  `json:"SystemAddress"`
	BodyID        int    `json:"BodyID"`
}

const EvtCarrierJumpCancelled = Name("CarrierJumpCancelled")

type CarrierJumpCancelled struct {
	EdEvent
	CarrierID int64 `json:"CarrierID"`
}

const EvtCarrierDecommission = Name("CarrierDecommission")

type CarrierDecommission struct {
	EdEvent
	CarrierID   int64 `json:"CarrierID"`
	ScrapRefund int   `json:"ScrapRefund"`
	ScrapTime   int   `json:"ScrapTime"`
}

const EvtCarrierCancelDecommission = Name("CarrierCancelDecommission")

type CarrierCancelDecommission struct {
	EdEvent
	CarrierID int64 `json:"CarrierID"`
}

const EvtCarrierDepositFuel = Name("CarrierDepositFuel")

type CarrierDepositFuel struct {
	EdEvent
	CarrierID int64 `json:"CarrierID"`
	Amount    int   `json:"Amount"`
	Total     int   `json:"Total"`
}

const EvtCarrierTradeOrder = Name("CarrierTradeOrder")

type CarrierTradeOrder struct {
	EdEvent
	CarrierID          int64  `json:"CarrierID"`
	BlackMarket        bool   `json:"BlackMarket"`
	Commodity          string `json:"Commodity"`
	CommodityLocalised string `json:"Commodity_Localised"`
	PurchaseOrder      int    `json:"PurchaseOrder"`
	Price              int    `json:"Price"`
}

const EvtCarrierStats = Name("CarrierStats")

type CarrierStats struct {
	EdEvent
	CarrierID           int64         `json:"CarrierID"`
	Callsign            string        `json:"Callsign"`
	Name                string        `json:"Name"`
	DockingAccess       string        `json:"DockingAccess"`
	AllowNotorious      bool          `json:"AllowNotorious"`
	FuelLevel           int           `json:"FuelLevel"`
	JumpRangeCurr       float64       `json:"JumpRangeCurr"`
	JumpRangeMax        float64       `json:"JumpRangeMax"`
	PendingDecommission bool          `json:"PendingDecommission"`
	SpaceUsage          SpaceUsage    `json:"SpaceUsage"`
	Finance             Finance       `json:"Finance"`
	Crew                []Crew        `json:"Crew"`
	ShipPacks           []ShipPacks   `json:"ShipPacks"`
	ModulePacks         []ModulePacks `json:"ModulePacks"`
}

type SpaceUsage struct {
	TotalCapacity      int `json:"TotalCapacity"`
	Crew               int `json:"Crew"`
	Cargo              int `json:"Cargo"`
	CargoSpaceReserved int `json:"CargoSpaceReserved"`
	ShipPacks          int `json:"ShipPacks"`
	ModulePacks        int `json:"ModulePacks"`
	FreeSpace          int `json:"FreeSpace"`
}
type Finance struct {
	CarrierBalance   int `json:"CarrierBalance"`
	ReserveBalance   int `json:"ReserveBalance"`
	AvailableBalance int `json:"AvailableBalance"`
	ReservePercent   int `json:"ReservePercent"`
	TaxRate          int `json:"TaxRate"`
}
type Crew struct {
	CrewRole  string `json:"CrewRole"`
	Activated bool   `json:"Activated"`
	Enabled   bool   `json:"Enabled"`
	CrewName  string `json:"CrewName"`
}
type ShipPacks struct {
	PackTheme string `json:"PackTheme"`
	PackTier  int    `json:"PackTier"`
}
type ModulePacks struct {
	PackTheme string `json:"PackTheme"`
	PackTier  int    `json:"PackTier"`
}
