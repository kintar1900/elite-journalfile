// ed-journal : A Go library for processing Elite:Dangerous journal files
// Copyright (C) 2020 Alec Lanter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Package event contains struct types that correspond to various JSON events written into the elite dangerous
// log directory, as well as convenience functions for parsing a single event from JSON, or an entire log file
// into a slice of Event instances
package event

import (
	"bufio"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"reflect"
	"time"
)

// Name provides a type-safe way to refer to the strings that define the names of events in the ED journal file.
type Name string

type eventConstructor func() Event

var eventConstructors map[Name]eventConstructor

func wrapConstructor(e interface{}) func() Event {
	return func() Event {
		evtPtr := reflect.New(reflect.TypeOf(e).Elem())
		return evtPtr.Elem().Addr().Interface().(Event)
	}
}

func init() {
	eventConstructors = make(map[Name]eventConstructor)

	eventConstructors[EvtFileheader] = wrapConstructor(&Fileheader{})
	eventConstructors[EvtRank] = wrapConstructor(&Rank{})
	eventConstructors[EvtSquadronStartup] = wrapConstructor(&SquadronStartup{})
	eventConstructors[EvtCommander] = wrapConstructor(&Commander{})
	eventConstructors[EvtDocked] = wrapConstructor(&Docked{})
	eventConstructors[EvtMarketBuy] = wrapConstructor(&MarketBuy{})
	eventConstructors[EvtMarketSell] = wrapConstructor(&MarketSell{})
	eventConstructors[EvtFSDJump] = wrapConstructor(&FSDJump{})

	eventConstructors[EvtCarrierJump] = wrapConstructor(&CarrierJump{})
	eventConstructors[EvtCarrierJumpRequest] = wrapConstructor(&CarrierJumpRequest{})
	eventConstructors[EvtCarrierJumpCancelled] = wrapConstructor(&CarrierJumpCancelled{})
	eventConstructors[EvtCarrierDecommission] = wrapConstructor(&CarrierDecommission{})
	eventConstructors[EvtCarrierCancelDecommission] = wrapConstructor(&CarrierCancelDecommission{})
	eventConstructors[EvtCarrierDepositFuel] = wrapConstructor(&CarrierDepositFuel{})
	eventConstructors[EvtCarrierTradeOrder] = wrapConstructor(&CarrierTradeOrder{})
	eventConstructors[EvtCarrierStats] = wrapConstructor(&CarrierStats{})
}

// EdEvent is composed into all other event types.  It contains fields to define the name of the event, and the
// timestamp corresponding to when the event was written.
type EdEvent struct {
	Time    time.Time `json:"timestamp"`
	EvtName Name      `json:"event"`
}

type Event interface {
	Timestamp() time.Time
	EventName() Name
}

func (e *EdEvent) Timestamp() time.Time {
	return e.Time
}

func (e *EdEvent) EventName() Name {
	return e.EvtName
}

type UnrecognizedEvent struct {
	eventName string
}

func (err UnrecognizedEvent) Error() string {
	return fmt.Sprintf("event '%s' is not recognized as a valid ed-journal event", err.eventName)
}

//noinspection GoUnusedGlobalVariable
var ErrUnrecognizedEvent = &UnrecognizedEvent{}

// ParseEvent takes an incoming line and attempts to Unmarshal it to a concrete Event implementation
//
// returns nil if unmarshalling fails, along with either a JSON unmarshalling error, or an UnrecognizedEvent error
// if the event type does not match one of this library's implemented events
func ParseEvent(line string) (Event, error) {
	rawData := make(map[string]interface{})
	err := json.Unmarshal([]byte(line), &rawData)
	if err != nil {
		return nil, fmt.Errorf("could not parse json: %w", err)
	}

	evtName, ok := rawData["event"].(string)
	if !ok {
		return nil, errors.New("could not find 'event' field in json")
	}

	constructor, ok := eventConstructors[Name(evtName)]
	if !ok {
		return nil, UnrecognizedEvent{eventName: evtName}
	}

	evt := constructor()
	err = json.Unmarshal([]byte(line), evt)
	if err != nil {
		return nil, fmt.Errorf("could not parse event json: %w", err)
	}

	return evt, nil
}

// ParseFile attempts to read each line of the specified file and parse it as a journal event.  It returns a slice of
// Event interfaces for all of the parsed events, and a count of lines which could not be parsed to an implemented
// Event type.
//
// returns (nil, 0, err) in the event of an error with the filesystem
func ParseFile(fileName string) ([]Event, int, error) {
	jsonFile, err := os.Open(fileName)
	if err != nil {
		return nil, 0, fmt.Errorf("failed to read file: %w", err)
	}
	invalids := 0
	var evt Event
	results := make([]Event, 0)
	scanner := bufio.NewScanner(jsonFile)
	for scanner.Scan() {
		evt, err = ParseEvent(scanner.Text())
		if err == nil {
			results = append(results, evt)
		} else {
			invalids++
		}
	}

	return results, invalids, nil
}
