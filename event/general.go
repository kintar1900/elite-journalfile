// ed-journal : A Go library for processing Elite:Dangerous journal files
// Copyright (C) 2020 Alec Lanter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package event

const EvtRank = Name("Rank")

type Rank struct {
	EdEvent
	Combat     int `json:"Combat"`
	Trade      int `json:"Trade"`
	Explore    int `json:"Explore"`
	Empire     int `json:"Empire"`
	Federation int `json:"Federation"`
	CQC        int `json:"CQC"`
}

const EvtFileheader = Name("Fileheader")

type Fileheader struct {
	EdEvent
	Part        int    `json:"part"`
	Language    string `json:"language"`
	Gameversion string `json:"gameversion"`
	Build       string `json:"build"`
}

const EvtSquadronStartup = Name("SquadronStartup")

type SquadronStartup struct {
	EdEvent
	SquadronName string `json:"SquadronName"`
	CurrentRank  int    `json:"CurrentRank"`
}

const EvtCommander = Name("Commander")

type Commander struct {
	EdEvent
	FID  string `json:"FID"`
	Name string `json:"Name"`
}

const EvtDocked = Name("Docked")

type Docked struct {
	EdEvent
	StationName                string             `json:"StationName"`
	StationType                string             `json:"StationType"`
	StarSystem                 string             `json:"StarSystem"`
	SystemAddress              int64              `json:"SystemAddress"`
	MarketID                   int64              `json:"MarketID"`
	StationFaction             StationFaction     `json:"StationFaction"`
	StationGovernment          string             `json:"StationGovernment"`
	StationGovernmentLocalised string             `json:"StationGovernment_Localised"`
	StationServices            []string           `json:"StationServices"`
	StationEconomy             string             `json:"StationEconomy"`
	StationEconomyLocalised    string             `json:"StationEconomy_Localised"`
	StationEconomies           []StationEconomies `json:"StationEconomies"`
	DistFromStarLS             float64            `json:"DistFromStarLS"`
}

type StationFaction struct {
	Name string `json:"Name"`
}

type StationEconomies struct {
	Name          string  `json:"Name"`
	NameLocalised string  `json:"Name_Localised"`
	Proportion    float64 `json:"Proportion"`
}

const EvtMarketBuy = Name("MarketBuy")

type MarketBuy struct {
	EdEvent
	MarketID      int64  `json:"MarketID"`
	Type          string `json:"Type"`
	TypeLocalised string `json:"Type_Localised"`
	Count         int    `json:"Count"`
	BuyPrice      int    `json:"BuyPrice"`
	TotalCost     int    `json:"TotalCost"`
}

const EvtMarketSell = Name("MarketSell")

type MarketSell struct {
	EdEvent
	MarketID     int    `json:"MarketID"`
	Type         string `json:"Type"`
	Count        int    `json:"Count"`
	SellPrice    int    `json:"SellPrice"`
	TotalSale    int    `json:"TotalSale"`
	AvgPricePaid int    `json:"AvgPricePaid"`
}

const EvtFSDJump = Name("FSDJump")

type FSDJump struct {
	EdEvent
	StarSystem                   string        `json:"StarSystem"`
	SystemAddress                int64         `json:"SystemAddress"`
	StarPos                      []float64     `json:"StarPos"`
	SystemAllegiance             string        `json:"SystemAllegiance"`
	SystemEconomy                string        `json:"SystemEconomy"`
	SystemEconomyLocalised       string        `json:"SystemEconomy_Localised"`
	SystemSecondEconomy          string        `json:"SystemSecondEconomy"`
	SystemSecondEconomyLocalised string        `json:"SystemSecondEconomy_Localised"`
	SystemGovernment             string        `json:"SystemGovernment"`
	SystemGovernmentLocalised    string        `json:"SystemGovernment_Localised"`
	SystemSecurity               string        `json:"SystemSecurity"`
	SystemSecurityLocalised      string        `json:"SystemSecurity_Localised"`
	Population                   int           `json:"Population"`
	Body                         string        `json:"Body"`
	BodyID                       int           `json:"BodyID"`
	BodyType                     string        `json:"BodyType"`
	JumpDist                     float64       `json:"JumpDist"`
	FuelUsed                     float64       `json:"FuelUsed"`
	FuelLevel                    float64       `json:"FuelLevel"`
	Factions                     []Factions    `json:"Factions"`
	SystemFaction                SystemFaction `json:"SystemFaction"`
}

type ActiveStates struct {
	State string `json:"State"`
}

type PendingStates struct {
	State string `json:"State"`
	Trend int    `json:"Trend"`
}

type Factions struct {
	Name               string          `json:"Name"`
	FactionState       string          `json:"FactionState"`
	Government         string          `json:"Government"`
	Influence          float64         `json:"Influence"`
	Allegiance         string          `json:"Allegiance"`
	Happiness          string          `json:"Happiness"`
	HappinessLocalised string          `json:"Happiness_Localised"`
	MyReputation       float64         `json:"MyReputation"`
	ActiveStates       []ActiveStates  `json:"ActiveStates,omitempty"`
	SquadronFaction    bool            `json:"SquadronFaction,omitempty"`
	HomeSystem         bool            `json:"HomeSystem,omitempty"`
	PendingStates      []PendingStates `json:"PendingStates,omitempty"`
}

type SystemFaction struct {
	Name         string `json:"Name"`
	FactionState string `json:"FactionState"`
}
