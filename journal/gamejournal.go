// ed-journal : A Go library for processing Elite:Dangerous journal files
// Copyright (C) 2020 Alec Lanter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Package journal provides a mechanism to emit Events from an RxGo (github.com/ReactiveX/RxGo/) Observable any time
// the game updates the files in the log directory.
package journal

import (
	"bufio"
	"errors"
	"fmt"
	"github.com/radovskyb/watcher"
	"gitlab.com/kintar1900/elite-journalfile/event"
	"log"
	"os"
	"regexp"
	"time"
)

const (
	edPathSlug = `Saved Games\Frontier Developments\Elite Dangerous`
)

func getEdJournalPath() (string, error) {
	userProfileDir := os.Getenv("USERPROFILE")
	if userProfileDir == "" {
		return "", errors.New("could not find USERPROFILE variable")
	} else {
		return fmt.Sprintf("%s\\%s", userProfileDir, edPathSlug), nil
	}
}

// GameJournal represents the set of log files produced by Elite as an observable stream of events.
// DO NOT directly instance this type.  Please use the NewJournal() function.
type GameJournal struct {
	journalDir   string
	watch        *watcher.Watcher
	currentFile  *os.File
	scanner      *bufio.Scanner
	eventChannel chan event.Event
	errorChannel chan error
	Events       <-chan event.Event
	Errors       <-chan error
}

// NewJournal creates and initializes a GameJournal.
func NewJournal() (gj GameJournal, err error) {
	gj = GameJournal{}

	path, err := getEdJournalPath()
	if err == nil {
		gj.journalDir = path
	}

	w := watcher.New()
	w.FilterOps(watcher.Create, watcher.Remove, watcher.Write, watcher.Move)

	err = w.Add(gj.journalDir)
	if err != nil {
		err = fmt.Errorf("could not create file watcher: %v", err)
		return
	}

	r := regexp.MustCompile(`Journal\..+\.log`)
	w.AddFilterHook(watcher.RegexFilterHook(r, false))

	gj.watch = w
	gj.eventChannel = make(chan event.Event, 25)
	gj.Events = gj.eventChannel
	gj.errorChannel = make(chan error, 1)
	gj.Errors = gj.errorChannel

	return
}

// StartMonitor starts the file watcher and begins emitting events to the Events channel
func (j GameJournal) StartMonitor() (err error) {
	err = j.watch.Start(time.Second)
	if err == nil {
		go j.monitorFunction()
	}

	return
}

// StopMonitor terminates the file watcher, preventing further events being emitted
func (j GameJournal) StopMonitor() {
	j.watch.Close()
}

func (j GameJournal) monitorFunction() {
	defer func() {
		if j.currentFile != nil {
			_ = j.currentFile.Close()
			j.currentFile = nil
		}
	}()

	for {
		select {
		case evt := <-j.watch.Event:
			switch evt.Op {
			case watcher.Create:
				if f, err := os.Open(evt.Path); err == nil {
					if j.currentFile != nil {
						_ = j.currentFile.Close()
					}
					j.currentFile = f
					j.scanner = bufio.NewScanner(j.currentFile)
					j.scanner.Split(bufio.ScanLines)
				}
			case watcher.Write:
				if j.currentFile == nil {
					continue
				}

				for j.scanner.Scan() {
					evt, err := event.ParseEvent(j.scanner.Text())
					if err == nil {
						// TODO: Isn't there a better way to do a non-blocking send without a success case?
						select {
						case j.eventChannel <- evt:
							// No op
						default:
							log.Println("channel full: dropped an event")
						}
					} else {
						log.Printf("error parsing event: %v\n", err)
					}
				}
			}
		case err := <-j.watch.Error:
			log.Printf("file watcher error: %v\n", err)
		case <-j.watch.Closed:
			return
		}
	}
}
